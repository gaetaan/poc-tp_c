import java.util.concurrent.Callable;

public class SortTask implements Callable<int[]>{

    private TaskController controller; 
    private int[] tableau;
    private int debut = 0, fin = 0;

    public SortTask(TaskController controller, int[] tableau, int debut, int fin){
        this.controller = controller;
        this.tableau = tableau;
        this.debut = debut;
        this.fin = fin;
    }

    public void sort(){
        if (debut < fin) {                             // S'il y a un seul élément, il n'y a rien à faire!
            int p = TriRapide.partitionner(tableau, debut, fin) ;      
            controller.addTask(new SortTask(controller, tableau, debut, p-1));
            controller.addTask(new SortTask(controller, tableau, p+1, fin));
        }
    }

    public int[] call(){
        sort();
        return null;
    }

}
