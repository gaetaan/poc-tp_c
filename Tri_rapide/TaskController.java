import java.util.ArrayList;
import java.util.concurrent.CompletionService;
import java.util.concurrent.*;
import java.util.concurrent.Future;

public class TaskController{
    private int nbTask = 0;
    
    private ExecutorService executeur = Executors.newFixedThreadPool(4);
    private CompletionService<int[]> esc = new ExecutorCompletionService<int[]>(executeur);

    public Future<int[]> addTask(SortTask task){
        Future<int[]> promesse = esc.submit(task);
        nbTask += 1;
        return promesse;
    }

    public void waitUntilEnd() throws Exception{
        System.out.println(nbTask);
        for (int i = 1; i < nbTask; i++){
            Future<int[]> promesse = esc.take();
           // int [] res = promesse.get();
           //System.out.println(i);
        }
    }

    public void stop(){
        executeur.shutdown();
    }

}