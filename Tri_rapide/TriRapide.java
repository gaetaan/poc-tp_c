// -*- coding: utf-8 -*-

import java.util.Random ;

public class TriRapide {
    static final int taille = 90 * 1_000_000 ;                   // Longueur du tableau à trier
    static final int [] tableau = new int[taille] ;         // Le tableau d'entiers à trier 
    static final int [] tableauP = new int[taille];         // Le tableau d'entiers à trier

    static final int borne = 10 * taille ;                  // Valeur maximale dans le tableau

    private static void echangerElements(int[] t, int m, int n) {
        int temp = t[m] ;
        t[m] = t[n] ;
        t[n] = temp ;
    }

    public static int partitionner(int[] t, int début, int fin) {
        int v = t[fin] ;                               // Choix (arbitraire) du pivot : t[fin]
        int place = début ;                            // Place du pivot, à droite des éléments déplacés
        for (int i = début ; i < fin ; i++) {            // Parcours du *reste* du tableau
            if (t[i] < v) {                            // Cette valeur t[i] doit être à droite du pivot
                echangerElements(t, i, place) ;        // On le place à sa place
                place++ ;                              // On met à jour la place du pivot
            }
        }
        echangerElements(t, place, fin) ;              // Placement définitif du pivot
        return place ;
    }

    public static void trierRapidement(int[] t, int début, int fin) {
        if (début < fin) {                             // S'il y a un seul élément, il n'y a rien à faire!
            int p = partitionner(t, début, fin) ;      
            trierRapidement(t, début, p-1) ;
            trierRapidement(t, p+1, fin) ;
        }
    }

    public static void afficher(int[] t, int début, int fin) {
        for (int i = début ; i <= début+3 ; i++) {
            System.out.print(" " + t[i]) ;
        }
        System.out.print("...") ;
        for (int i = fin-3 ; i <= fin ; i++) {
            System.out.print(" " + t[i]) ;
        }
        System.out.print("\n");
    }

    public static boolean egaux(int[] t1, int[] t2, int taille){
        boolean estEgaux = true;
        for(int i = 0; i < taille && estEgaux; i++){
            if(t1[i] != t2[i]){
                estEgaux = false;
            }
        }
        return estEgaux;
    }

    public static void copieTableau(int[] t1, int[] t2, int taille){
        for(int i = 0; i < taille; i++){
            t2[i] = t1[i];
        }
    }
    public static void main(String[] args) {
        Random alea = new Random() ;
        for (int i = 0 ; i < taille ; i++) {                          // Remplissage aléatoire du tableau
            tableau[i] = alea.nextInt(2*borne) - borne ;            
        }

        copieTableau(tableau, tableauP, taille);

        System.out.print("Tableau initial : ") ;
        afficher(tableau, 0, taille -1) ;                         // Affiche le tableau à trier

        System.out.println("Démarrage du tri rapide Séquentiel.") ;
        long débutDuTriS = System.nanoTime();

        trierRapidement(tableau, 0, taille-1) ;                   // Tri du tableau

        long finDuTriS = System.nanoTime();
        long duréeDuTriS = (finDuTriS - débutDuTriS) / 1_000_000 ;
        System.out.print("Tableau trié Séquentiel :") ; 
        afficher(tableau, 0, taille -1) ;                         // Affiche le tableau obtenu
        System.out.println("Obtenu en " + duréeDuTriS + " millisecondes.") ;

        TaskController controller = new TaskController();

        System.out.println("Démarrage du tri rapide Parrallele.") ;
        long débutDuTriP = System.nanoTime();

        trierRapidement(tableauP, 0, taille-1) ;                   // Tri du tableau
        controller.addTask(new SortTask(controller, tableauP, 0, taille-1));
       
        try {
            controller.waitUntilEnd();
        } catch (Exception e) {
            e.printStackTrace();
        }

        long finDuTriP = System.nanoTime();
        long duréeDuTriP = (finDuTriP - débutDuTriP) / 1_000_000 ;
        System.out.print("Tableau trié Parallele :") ; 
        afficher(tableauP, 0, taille -1) ;                         // Affiche le tableau obtenu
        System.out.println("Obtenu en " + duréeDuTriP + " millisecondes.") ;

        if(egaux(tableau, tableauP, taille)){
            System.out.println("Les tableaux sont égaux.");
        }else {
            System.out.println("Les tableaux sont différents.");
        }

        controller.stop();

    }
}

/*
  $ make
  javac *.java
  $ java TriRapide
  Tableau initial :  4967518 -8221265 -951337 4043143... -4807623 -1976577 -2776352 -6800164
  Démarrage du tri rapide.
  Tableau trié :  -9999981 -9999967 -9999957 -9999910... 9999903 9999914 9999947 9999964
  obtenu en 85 millisecondes.
*/ 
